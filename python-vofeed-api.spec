Summary: Python API for the WLCG experiments topological feed
Name: python3-vofeed-api
Version: 0.1.16
Release: 2%{?dist}
Source: python-vofeed-api-%{version}.tar.gz
License: ASL 2.0
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Url: https://gitlab.cern.ch/etf/vofeed
%if 0%{?el7}
Requires: python-requests
%else
Requires: python3-requests
%endif
%if 0%{?el7}
BuildRequires: python-setuptools
%else
BuildRequires: python3-setuptools
%endif
%description

Python API for the WLCG experiments topological feed


%prep
%autosetup -n python-vofeed-api-%{version}

%build
%if 0%{?el7}
python setup.py build
%else
%py3_build
%endif

%install
%if 0%{?el7}
python setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
%else
%{__python3} setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%defattr(-,root,root)
%doc README.md
