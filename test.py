import logging
import sys
from vofeed import api
from collections import defaultdict

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def cmp(new, old, vo=None):
    ff1 = api.VOFeed(new, vo=vo) 
    ff2 = api.VOFeed(old)
    print('f1:', new)
    print('f2:', old)
  
    s1 = sorted(ff1.get_services())
    s1 = [(i[0], i[1]) for i in s1]
    s2 = sorted(ff2.get_services())
    s2 = [(i[0], i[1]) for i in s2]
    s1f = filter(lambda x: x[1] in ('HTCONDOR-CE', 'ARC-CE'), s1)
    s2f = filter(lambda x: x[1] in ('HTCONDOR-CE', 'ARC-CE'), s2)
    print('f1 - f2:', set(s1f) - set(s2f))
    print('f2 - f1:', set(s2f) - set(s1f))

    print('ce_resources:')
    qd = defaultdict(list)
    for h, f in sorted(s1f):
        qq1 = ff1.get_ce_resources(h, f)
        qq1 = [i[1] for i in qq1]
        if qq1:
             qd[h].append(sorted(qq1))
            
    for h, f in sorted(s2f):
        if vo == 'lhcb':
            qq2 = ff2.get_queues(h, f)
        else:
            qq2 = ff2.get_ce_resources(h, f)
        qq2 = [i[1] for i in qq2]
        if qq2:
            qd[h].append(sorted(qq2))
    for k, v in sorted(qd.items()):
        if len(v) == 1:
            print('nomatch:', k, v)
            continue
        if set(v[0]) - set(v[1]):
            print('mismatch:', k, v)

    print('groups:')
    if vo == 'atlas':
        os2 = ff2.get_groups("ATLAS_Site")
    elif vo == 'lhcb':
        os2 = ff2.get_groups("LHCb_Site")
    elif vo == 'alice':
        os2 = ff2.get_groups("ALICE_Site")
    os1 = ff1.get_groups()
    print('f1 - f2:', set(os1.keys()) - set(os2.keys()))
    print('f2 - f1:', set(os2.keys()) - set(os1.keys()))
    for k, v in sorted(os1.items()):
        if k in os2.keys() and v - os2[k]:
            print('mismatch:', k, v, os2[k])

    #print('se_resources:')
    #s1f = filter(lambda x: x[1] in ('WEBDAV', 'SRM', 'SRMv2', 'DCAP', 'CVMFS', 'GRIDFTP', 'XROOTD'), s1)
    #s2f = filter(lambda x: x[1] in ('WEBDAV', 'SRM', 'SRMv2', 'DCAP', 'CVMFS', 'GRIDFTP', 'XROOTD'), s2)
    #qd = defaultdict(list)
    #for h, f in sorted(s1f):
    #    qq1 = ff1.get_se_resources(h, f)
    #    qq1 = [i[1] for i in qq1]
    #    if qq1:
    #        qd[h].append(sorted(qq1))
#
#    for h, f in sorted(s2f):
#        qq2 = ff2.get_se_resources(h, f)
#        qq2 = [i[1] for i in qq2]
#        if qq2:
#            qd[h].append(sorted(qq2))
#    for k, v in sorted(qd.items()):
#        if len(v) == 1:
#            print('nomatch:', k, v)
#            continue
#        if set(v[0]) - set(v[1]):
#            print('mismatch:', k, v)


#log = logging.getLogger()
#log.setLevel(logging.INFO)
#formatter = logging.Formatter(fmt='%(asctime)s %(levelname)s %(module)s[%(process)d]: %(message)s',
#                                       datefmt='%b %d %H:%M:%S')
#fh = logging.StreamHandler(stream=sys.stdout)
#fh.setFormatter(formatter)
#log.addHandler(fh)

print('ATLAS')
cmp('https://wlcg-cric.cern.ch/api/core/service/query/list/?json&is_monitored=1',
    'https://atlas-cric.cern.ch/api/atlas/vofeed/?xml&service_is_monitored=1', vo='atlas')

#cmp('https://wlcg-cric.cern.ch/api/core/service/query/list/?json&is_monitored=1',
#    'https://atlas-cric.cern.ch/api/core/service/query/list/?json&is_monitored=1', vo='atlas')

print('LHCb')
cmp('https://wlcg-cric.cern.ch/api/core/service/query/list/?json',
    'http://wlcg-cric.cern.ch/api/wlcg/vofeed/lhcb/?xml', vo='lhcb')

print('ALICE')
cmp('https://wlcg-cric.cern.ch/api/core/service/query/list/?json',
    'http://wlcg-cric.cern.ch/api/wlcg/vofeed/alice/?xml', vo='alice')
